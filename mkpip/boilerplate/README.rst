%(name)s
%(underline)s

%(desc)s

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ %(name)s

Use --help/-h to view info on the arguments::

    $ %(name)s --help

Release Notes
-------------

:0.0.1:
    Project created
